const { app, Menu, ipcRenderer } = require('electron')
const { __DEV__ } = require('./util')

export const defaultMenu = [
  {
    label: '设置',
    submenu: [
      {
        label: '打印机',
        role: 'cut',
        click() {
          console.log('打印机')
        }
      },
      // {
      //   label: '复制',
      //   role: 'copy'
      // },
      // {
      //   label: '粘贴',
      //   role: 'paste'
      // },
      // {
      //   label: '删除',
      //   role: 'delete'
      // },
      // {
      //   label: '全选',
      //   role: 'selectall'
      // }
    ]
  },
  // {
  //   label: '打印机',
  //   submenu: [
  //     {
  //       label: '设置打印机',
  //       click: function(item, focusedWindow) {
  //         // console.log('设置打印机')
  //         // this.testApp()
  //         app.emit('testApp')
  //       }
  //     }
  //   ]
  // },
  {
    label: '视图',
    submenu: [
      {
        label: 'Reload',
        accelerator: 'CmdOrCtrl+R',
        click: function(item, focusedWindow) {
          if (focusedWindow) focusedWindow.reload()
        }
      },
      {
        label: 'Toggle Full Screen',
        accelerator: (function() {
          if (process.platform == 'darwin') return 'Ctrl+Command+F'
          else return 'F11'
        })(),
        click: function(item, focusedWindow) {
          if (focusedWindow) {
            focusedWindow.setFullScreen(!focusedWindow.isFullScreen())
          }
        }
      },
      {
        label: 'Toggle Developer Tools',
        accelerator: (function() {
          if (process.platform == 'darwin') return 'Alt+Command+I'
          else return 'Ctrl+Shift+I'
        })(),
        click: function(item, focusedWindow) {
          if (focusedWindow) focusedWindow.toggleDevTools()
        }
      }
    ]
  },
  {
    label: '帮助',
    role: 'help',
    submenu: [
      {
        label: '关于',
        click() {
          console.log('关于')
        }
      }
    ]
  }
]
if (__DEV__) {
  defaultMenu[0].submenu.push({
    label: '开发者工具',
    role: 'toggledevtools'
  })
}

if (process.platform === 'darwin') {
  defaultMenu.unshift({
    label: app.getName(),
    submenu: [
      {
        label: '退出',
        role: 'quit'
      }
    ]
  })
}
export const setMenu = (options = []) => {
  const menus = [...defaultMenu, ...options]
  Menu.setApplicationMenu(Menu.buildFromTemplate(menus))
}
